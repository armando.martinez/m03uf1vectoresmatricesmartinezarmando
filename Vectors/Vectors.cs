﻿/*
 * AUTHOR: Armando Martínez
 * DATE: 23/11/2022
 * DESCRIPTION: Conjunt d'exercicis de vectors
 */

using System;

namespace Vectors
{
    class Vectors
    {
        // Mostra el dia de la setmana corresponent a un número (del 0 al 6)
        public void DayOfWeek() 
        {
            string[] days = { "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge" };
            Console.Write("\nEscriu un número del 0 al 6: ");
            int dayNum = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(days[dayNum]);
        }
        // Demana una alineació de jugadors
        public void PlayerNumbers() 
        {
            int[] alignment = new int[5];
            Console.Write("\nIntrodueix un número de jugador: ");
            alignment[0] = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i < 5; i++)
            {
                Console.Write("Introdueix un altre: ");
                alignment[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.Write("[");
            for (int i = 0; i < alignment.Length - 1; i++)
            {
                Console.Write($"{alignment[i]}, ");
            }
            Console.WriteLine($"{alignment[alignment.Length - 1]}]");
        }
        // Programa que et mostra un candidat d'acord al seu número de llista
        public void CandidatesList() 
        {
            Console.WriteLine("\nQuants candidats volem?");
            int num = Convert.ToInt32(Console.ReadLine());
            string[] candidats = new string[num];
            Console.WriteLine("A continuació introdueix els noms dels candidats en ordre:");
            for (int i = 0; i < num; i++) { candidats[i] = Console.ReadLine(); }
            int position = 0;
            while (position != -1)
            {
                Console.Write("Quin número de candidat vols? (Introdueix -1 per terminar): ");
                position = Convert.ToInt32(Console.ReadLine());
                if (position != -1) { Console.WriteLine(candidats[position-1]); }
            }
        }
        // Dona la lletra en la posició X d'una paraula
        public void LetterInWord()
        {
            Console.Write("Introdueix una paraula: ");
            string word = Console.ReadLine();
            Console.Write("Introdueix un número: ");
            int index = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(word[index]);
        }
        // En un vector de mida 50, substitueix 4 valors
        public void AddValuesToList() 
        {
            float[] vector = new float[50];
            for (int i = 0; i < 50; i++) { vector[i] = 0.0f; }
            vector[0] = 31.0f;
            vector[1] = 56.0f;
            vector[19] = 12.0f;
            vector[49] = 79.0f;
            Console.Write("[");
            for (int i = 0; i < 49; i++) { Console.Write($"{vector[i]}.0, "); }
            Console.WriteLine($"{vector[vector.Length - 1]}.0]");
        }
        // Intercanvia el primer valor d'un vector per l'últim
        public void Swap()
        {
            int[] vector = new int[4];
            Console.WriteLine("Introdueix 4 números: ");
            for (int i = 0; i < 4; i++)
            {
                vector[i] = Convert.ToInt32(Console.ReadLine());
            }
            int aux = vector[0];
            vector[0] = vector[3];
            vector[3] = aux;
            Console.Write("[");
            for (int i = 0; i < vector.Length - 1; i++) { Console.Write($"{vector[i]}, "); }
            Console.WriteLine($"{vector[vector.Length - 1]}]");
        }
        // Emula un candat amb 8 botons que poden estar o no clickats. Al final et mostra quins estàn clickats i quins no.
        public void PushButtonPadlockSimulator()
        {
            bool[] botons = { false, false, false, false, false, false, false, false };
            int select;
            Console.WriteLine("Tens 8 botons, introdueix números del 1 al 8 per activarlos i desactivarlos. Per terminar escriu -1");
            do
            {
                select = Convert.ToInt32(Console.ReadLine());
                if (select != -1) { botons[select - 1] = !botons[select - 1]; }
            } while (select != -1);
            Console.Write("[");
            for (int i = 0; i < botons.Length - 1; i++) { Console.Write($"{botons[i]}, "); }
            Console.WriteLine($"{botons[botons.Length - 1]}]");
        }
        // Tens 10 caixes. Cada vegada que s'obre una queda registrat i al final del dia es veu quantes vegades s'ha obert cada caixa.
        public void BoxesOpenedCounter() 
        {
            int[] caixes = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int select;
            Console.WriteLine("Tens 10 caixes, introdueix números del 1 al 10 per obrirles una vegada. Per terminar i mostrar quantes vegades s'han obert escriu -1");
            do
            {
                select = Convert.ToInt32(Console.ReadLine());
                if (select != -1) { caixes[select - 1] += 1; }
            } while (select != -1);
            Console.Write("[");
            for (int i = 0; i < caixes.Length - 1; i++) { Console.Write($"{caixes[i]}, "); }
            Console.WriteLine($"{caixes[caixes.Length - 1]}]");
        }
        // Et diu el valor més petit de 10
        public void MinOf10Values() 
        {
            int[] vector = new int[10];
            Console.WriteLine("Introdueix 10 números enters: ");
            for (int i = 0; i < 10; i++)
            {
                vector[i] = Convert.ToInt32(Console.ReadLine());
            }
            int min = vector[0];
            foreach (int val in vector){ if (val < min) { min = val; } }
            Console.WriteLine($"El valor mínim es {min}");
        }
        // Et diu si n'hi ha un multiple de 7 al Vector "values".
        public void IsThereAMultipleOf7()
        {
            int[] values = {4, 8, 9, 40, 54, 84, 40, 6, 84, 1, 1, 68, 84, 68, 4, 840, 684, 25, 40, 98,
                54, 687, 31, 4894, 468, 46, 84687, 894, 40, 846, 1681, 618, 161, 846, 84687, 6, 848};
            bool isMultipleof7 = false;
            foreach (int val in values){ if (val % 7 == 0) { isMultipleof7 = true; } }
            Console.WriteLine(isMultipleof7);
        }
        // Busca si un número existeix dins d'un vector
        public void SearchInOrdered() 
        {
            Console.WriteLine("\nQuants números vols introduir?");
            int num = Convert.ToInt32(Console.ReadLine());
            int[] vector = new int[num];
            for (int i = 0; i < vector.Length; i++) { vector[i] = Convert.ToInt32(Console.ReadLine()); }
            Console.Write("Quin número vols buscar? ");
            int input = Convert.ToInt32(Console.ReadLine());
            bool isInVector = false;
            foreach (int val in vector) { if (val == input) { isInVector = true; } }
            Console.WriteLine(isInVector);
        }
        // L'usuari introdueix 10 números i es mostren en l'ordre invers
        public void InverseOrder()
        {
            Console.WriteLine("\nIntrodueix 10 números");
            int[] vector = new int[10];
            for (int i = vector.Length - 1; i >= 0; i--) { vector[i] = Convert.ToInt32(Console.ReadLine()); }
            Console.Write("[");
            for (int i = 0; i < vector.Length - 1; i++) { Console.Write($"{vector[i]}, "); }
            Console.WriteLine($"{vector[vector.Length - 1]}]");
        }
        // Et diu si una paraula es o no un palindrom
        public void Palindrome()
        {
            Console.Write("\nIntrodueix una paraula: ");
            string input = Console.ReadLine();
            string palindrome = "";
            for (int i = input.Length - 1; i >= 0; i--) { palindrome += input[i]; }
            if (input == palindrome) { Console.WriteLine("Es un palindrom!"); } else
            { Console.WriteLine("NO es una palindrom!"); }
        }
        // Et diu si un llistat de números està ordenat
        public void ListSortedValues()
        {
            Console.WriteLine("\nQuants números vols introduir?");
            int num = Convert.ToInt32(Console.ReadLine());
            int[] vector = new int[num];
            Console.WriteLine($"Introdueix {num} números: ");
            for (int i = 0; i < vector.Length; i++) { vector[i] = Convert.ToInt32(Console.ReadLine()); }
            int previousVal = vector[0];
            bool isOrdered = true;
            foreach (int val in vector)
            {
                if (val < previousVal)
                {
                    isOrdered = false;
                }
                previousVal = val;
            }
            if (isOrdered == true) { Console.WriteLine("Has introduit els números ORDENATS"); } else
            { Console.WriteLine("Has introduit els números DESORDENATS"); }
        }
        // Et diu si un llistat de nombres es capicua
        public void CapICuaValues() 
        {
            Console.WriteLine("\nQuants números vols introduir?");
            int num = Convert.ToInt32(Console.ReadLine());
            int[] vector = new int[num];
            Console.WriteLine($"Introdueix {num} números: ");
            for (int i = 0; i < vector.Length; i++) { vector[i] = Convert.ToInt32(Console.ReadLine()); }
            int[] capicua = new int[num];
            for (int i = capicua.Length - 1; i >= 0; i--) { capicua[i] = vector[(vector.Length - 1) - i]; }
            bool isCapicua = true;
            for (int i = 0; i < vector.Length; i++) { if (vector[i] != capicua[i]) { isCapicua = false; } }
            if (isCapicua) { Console.WriteLine("Aquest llistat de números es capicua"); } else
            { Console.WriteLine("Aquest llistat de números NO es capicua"); }
        }
        // Et diu si dos vectors son iguals
        public void ListSameValues()
        {
            Console.WriteLine("\nQuants números vols introduir?");
            int num = Convert.ToInt32(Console.ReadLine());
            int[] vector = new int[num];
            Console.WriteLine($"Introdueix {num} números: ");
            for (int i = 0; i < vector.Length; i++) { vector[i] = Convert.ToInt32(Console.ReadLine()); }
            int[] vector2 = new int[num];
            Console.WriteLine($"Introdueix {num} números: ");
            for (int i = 0; i < vector2.Length; i++) { vector2[i] = Convert.ToInt32(Console.ReadLine()); }
            bool isSameValues = true;
            for (int i = 0; i < vector.Length; i++) { if (vector[i] != vector2[i]) { isSameValues = false; } }
            if (isSameValues) { Console.WriteLine("Aquest llistat de números es igual"); }
            else
            { Console.WriteLine("Aquest llistat de números NO es igual"); }
        }
        // Suma tots els valors donats per un usuari
        public void ListSumValues()
        {
            Console.WriteLine("\nQuants números vols introduir?");
            int num = Convert.ToInt32(Console.ReadLine());
            int[] vector = new int[num];
            Console.WriteLine($"Introdueix {num} números: ");
            int suma = 0;
            for (int i = 0; i < vector.Length; i++) { 
                vector[i] = Convert.ToInt32(Console.ReadLine());
                suma += vector[i];
            }
            Console.WriteLine(suma);
        }
        // Et retorna el preu de 10 articles sumant el 21% d'IVA
        public void IvaPrices()
        {
            double[] vector = new double[10];
            Console.WriteLine($"Introdueix {10} números: ");
            for (int i = 0; i < vector.Length; i++) { vector[i] = Convert.ToDouble(Console.ReadLine()); }
            for (int i = 0; i < vector.Length; i++) { Console.WriteLine($"{vector[i]} IVA = {vector[i] + (vector[i] * 0.21)}"); }
        }
        // Mostra el rati de creixement de contagis 
        public void CovidGrowRate()
        {
            Console.WriteLine("\nQuantes setmanes vols introduir?");
            int num = Convert.ToInt32(Console.ReadLine());
            int[] vector = new int[num];
            double[] growth = new double[num - 1];
            Console.WriteLine($"Introdueix el número de casos en {num} dies: ");
            vector[0] = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i < num; i++)
            {
                vector[i] = Convert.ToInt32(Console.ReadLine());
                growth[i - 1] = Convert.ToDouble(vector[i]) / vector[i - 1];
            }
            Console.Write("[");
            for (int i = 0; i < growth.Length - 1; i++) { Console.Write($"{growth[i]}, "); }
            Console.WriteLine($"{growth[growth.Length - 1]}]");

        }
        // Mostra la velocitat que recorreix una bicicleta amb velocitat X
        public void BicicleDistance()
        {
            Console.WriteLine("\nA quina velocitat (m/s) va la bici?");
            double num = Convert.ToDouble(Console.ReadLine());
            for (int i = 1; i <= 10; i++) { Console.Write($"{num * i} "); }
        }
        // Calcula quin caracter dins d'un vector es més a prop de la mitjana
        public void ValueNearAvg() 
        {
            Console.WriteLine("\nQuants números vols introduir?");
            int num = Convert.ToInt32(Console.ReadLine());
            int[] vector = new int[num];
            Console.WriteLine($"Introdueix {num} números: ");
            for (int i = 0; i < vector.Length; i++) { vector[i] = Convert.ToInt32(Console.ReadLine()); }
            int sum = 0;
            foreach (int val in vector) { sum += val; }
            double average = sum / Convert.ToDouble(num);
            double distance;
            int closest = vector[0];
            foreach (int val in vector)
            {
                distance = val - average;
                if (distance < 0) { distance *= -1; }
                if (distance < closest) { closest = val; }
            }
            Console.WriteLine($"El valor més proper a la mitjana es {closest}");
        }
        // Comprova si un número ISBN es correcte
        public void Isbn()
        {
            int[] vector = new int[10];
            Console.WriteLine($"Introdueix els 10 números del ISBN: ");
            for (int i = 0; i < vector.Length; i++) { vector[i] = Convert.ToInt32(Console.ReadLine()); }
            int sum = 0;
            for (int i = 0; i < vector.Length - 1; i++) { sum += (vector[i] * (i + 1)); }
            bool correct = false;
            if (sum % 11 == vector[vector.Length - 1]) { correct = true; }
            Console.WriteLine(correct);
        }
        // Comprova si un número ISBN13 es correcte
        public void Isbn13()
        {
            int[] vector = new int[13];
            Console.WriteLine($"Introdueix els 13 números del ISBN13: ");
            for (int i = 0; i < vector.Length; i++) { vector[i] = Convert.ToInt32(Console.ReadLine()); }
            bool ticTac = false;
            int sum = 0;
            foreach (int val in vector)
            {
                if (ticTac == false) { sum += val; }
                else { sum += (val * 3); }
                ticTac = !ticTac;
            }
            if (sum % 10 == 0) { Console.WriteLine(true); } else { Console.WriteLine(false); }
        }
        public bool Menu(bool end)
        {
            string[] exercicis = { " 0 - Exit", " 1 - DayOfWeek", " 2 - PlayerNumbers", " 3 - CandidatesList",
                " 4 - LetterInWord", " 5 - AddValuesToList", " 6 - Swap", " 7 - PushButtonPadlockSimulator",
                " 8 - BoxesOpenedCounter", " 9 - MinOf10Values", "10 - IsThereAMultipleOf7", "11 - SearchInOrdered",
                "12 - InverseOrder", "13 - Palindrome", "14 - ListSortedValues", "15 - CapICuaValues",
                "16 - ListSameValues", "17 - ListSumValues", "18 - IvaPrices", "19 - CovidGrowRate",
                "20 - BicicleDistance", "21 - ValueNearAvg", "22 - Isbn", "23 - Isbn13"
            };
            Console.WriteLine("Quin exercici vols executar?\n");
            for (int i = 0; i < 24; i++)
            {
                Console.WriteLine(exercicis[i]);
            }
            Console.Write("\n Escriu un número: ");
            string option = Console.ReadLine();
            switch (option)
            {
                case "0":
                    end = !end;
                    break;
                case "1":
                    DayOfWeek();
                    break;
                case "2":
                    PlayerNumbers();
                    break;
                case "3":
                    CandidatesList();
                    break;
                case "4":
                    LetterInWord();
                    break;
                case "5":
                    AddValuesToList();
                    break;
                case "6":
                    Swap();
                    break;
                case "7":
                    PushButtonPadlockSimulator();
                    break;
                case "8":
                    BoxesOpenedCounter();
                    break;
                case "9":
                    MinOf10Values();
                    break;
                case "10":
                    IsThereAMultipleOf7();
                    break;
                case "11":
                    SearchInOrdered();
                    break;
                case "12":
                    InverseOrder();
                    break;
                case "13":
                    Palindrome();
                    break;
                case "14":
                    ListSortedValues();
                    break;
                case "15":
                    CapICuaValues();
                    break;
                case "16":
                    ListSameValues();
                    break;
                case "17":
                    ListSumValues();
                    break;
                case "18":
                    IvaPrices();
                    break;
                case "19":
                    CovidGrowRate();
                    break;
                case "20":
                    BicicleDistance();
                    break;
                case "21":
                    ValueNearAvg();
                    break;
                case "22":
                    Isbn();
                    break;
                case "23":
                    Isbn13();
                    break;
                default:
                    Console.WriteLine("\nOpció incorrecta!\n");
                    break;
            }
            Console.WriteLine("\n\n[PRESS ENTER TO CONTINUE]");
            Console.ReadLine();
            Console.Clear();
            return end;
        }
        static void Main()
        {
            var menu = new Vectors();
            bool end = false;
            while (!end) { end = menu.Menu(end); }
        }
    }
}
