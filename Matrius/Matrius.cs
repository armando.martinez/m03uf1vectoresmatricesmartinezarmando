﻿/*
 * AUTHOR: Armando Martínez
 * DATE: 11/2022
 * DESCRIPTION: Conjunt d'exercicis de matrius
 */

using System;

namespace Matrius
{
    class Matrius
    {
        // Demana unes coordenades i et diu si has tocat un vaixell (X) o aigua (0)
        public void SimpleBattleshipResult()
        {
            // EMPLENAR UNA MATRIU
            char[,] matrix = new char[7, 7];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++) { matrix[i, j] = '0'; }
            }
            // SUBSTITUIR ELEMENTS EN UNA MATRIU
            int[] imatrix = { 0, 0, 0, 1, 1, 2, 3, 3, 3, 3, 4, 5, 6 };
            int[] jmatrix = { 0, 1, 6, 2, 6, 6, 1, 2, 3, 6, 4, 4, 0 };
            for (int i = 0; i < imatrix.Length; i++)
            {
                matrix[imatrix[i], jmatrix[i]] = 'X';
            }
            // MOSTRAR MATRIU PER PANTALLA
            int count = 0;
            foreach (char val in matrix)
            {
                if (count == matrix.GetLength(0)) { Console.WriteLine(); count = 0; }
                Console.Write($"{val} ");
                count++;
            }
            //
            Console.WriteLine("\nDona'm coordenades X e Y");
            Console.Write("X: ");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Y: ");
            int y = Convert.ToInt32(Console.ReadLine());
            if (matrix[x, y] == 'X') { Console.WriteLine("TOCAT!"); }
            else { Console.WriteLine("Aigua..."); }
        }

        // Suma tots els elements d'una matriu
        public void MatrixElementSum()
        {
            int[,] matrix = { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 64 } };
            int suma = 0;
            foreach (int val in matrix) { suma += val; }
            Console.WriteLine(suma);
        }

        // Et demana coordenades per obrir caixes. Suma totes les vegades que s'obren
        public void MatrixBoxesOpenedCounter()
        {
            int[,] matrix = new int[4, 4];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++) { matrix[i, j] = 0; }
            }
            Console.WriteLine("\nEscriu coordenades per obrir les caixes, introdueix -1 a la X per terminar.");
            int x; int y;
            do
            {
                Console.Write("X: ");
                x = Convert.ToInt32(Console.ReadLine());
                if (x != -1)
                {
                    Console.Write("Y: ");
                    y = Convert.ToInt32(Console.ReadLine());
                    matrix[x, y]++;
                }
            } while (x != -1);
            int count = 0;
            foreach (int val in matrix)
            {
                if (count == matrix.GetLength(0)) { Console.WriteLine(); count = 0; }
                Console.Write($"{val} ");
                count++;
            }
        }

        // Diu true si a la matriu n'hi ha cap multiple de 13, de lo contrari diu false
        public void MatrixThereADiv13()
        {
            int[,] matrix = { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 62 } };
            bool isDiv13 = false;
            foreach (int val in matrix)
            {
                if (val % 13 == 0) { isDiv13 = true; }
            }
            Console.WriteLine(isDiv13);
        }

        // Troba el punt més alt d'un mapa de bits i et diu les seves coordenades
        public void HighestMountainOnMap()
        {
            double[,] map = { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };
            double biggest = map[0, 0];
            int x = 0;
            int y = 0;
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] > biggest)
                    {
                        biggest = map[i, j];
                        x = i; y = j;
                    }
                }
            }
            int count = 0;
            foreach (double val in map)
            {
                if (count == map.GetLength(0)) { Console.WriteLine(); count = 0; }
                Console.Write($"{val} ");
                count++;
            }
            Console.WriteLine($"\n\nEl punt més alt es a {biggest} metres, en les coordenades {x}, {y}");
        }

        // En un mapa de bits transforma les dades a peus, troba el punt més alt i et diu les seves coordenades
        public void HighestMountainScaleChange()
        {
            double[,] map = { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++) { map[i, j] *= 3.2808; }
            }
            double biggest = map[0, 0];
            int x = 0;
            int y = 0;
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] > biggest)
                    {
                        biggest = map[i, j];
                        x = i; y = j;
                    }
                }
            }
            int count = 0;
            foreach (double val in map)
            {
                if (count == map.GetLength(0)) { Console.WriteLine(); count = 0; }
                Console.Write($"{Math.Round(val, 3)}\t");
                count++;
            }
            Console.WriteLine($"\n\nEl punt més alt es a {Math.Round(biggest, 3)} peus, en les coordenades {x}, {y}");
        }

        // Suma de dues matrius
        public void MatrixSum()
        {
            Console.WriteLine("Dimensions de les matrius: ");
            Console.Write("X: ");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Y: ");
            int y = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Matriu 1 (Introdueix valor per valor)");
            int[,] vector1 = new int[y, x];
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    vector1[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            Console.WriteLine("Matriu 2 (Introdueix valor per valor)");
            int[,] vector2 = new int[y, x];
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    vector2[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            int[,] resultat = new int[y, x];
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    resultat[i, j] = vector1[i, j] + vector2[i, j];
                }
            }
            Console.WriteLine("RESULTAT: ");
            int count = 0;
            foreach (int val in resultat)
            {
                if (count == resultat.GetLength(0)) { Console.WriteLine(); count = 0; }
                Console.Write($"{val} ");
                count++;
            }
        }

        // Et mostra els possibles moviments per una torre en una posició d'un tauler d'escacs
        public void RookMoves()
        {
            char[,] chess = new char[8, 8];
            for (int i = 0; i < chess.GetLength(0); i++)
            {
                for (int j = 0; j < chess.GetLength(1); j++) { chess[i, j] = 'x'; }
            }
            Console.WriteLine("\nIntrodueix una casella del tauler d'escacs (De A1 fins a H8)");
            char x = Convert.ToChar(Console.Read());
            int y = Convert.ToInt32(Console.ReadLine());
            int iT = (y - 8) * -1;
            int jT = 0;
            switch (x)
            {
                case 'a':
                case 'A':
                    jT = 0;
                    break;
                case 'b':
                case 'B':
                    jT = 1;
                    break;
                case 'c':
                case 'C':
                    jT = 2;
                    break;
                case 'd':
                case 'D':
                    jT = 3;
                    break;
                case 'e':
                case 'E':
                    jT = 4;
                    break;
                case 'f':
                case 'F':
                    jT = 5;
                    break;
                case 'g':
                case 'G':
                    jT = 6;
                    break;
                case 'h':
                case 'H':
                    jT = 7;
                    break;
            }
            int count = 0;
            for (int i = 0; i < chess.GetLength(0); i++)
            {
                for (int j = 0; j < chess.GetLength(1); j++)
                {
                    if (i == iT || j == jT) { chess[i, j] = 't'; }
                }
            }
            chess[iT, jT] = 'T';
            foreach (char val in chess)
            {
                if (count == chess.GetLength(0)) { Console.WriteLine(); count = 0; }
                Console.Write($"{val} ");
                count++;
            }
            Console.WriteLine("\nT = Torre\nt = Moviments possibles");
        }

        // Donada una matriu cuadrada, el programa diu si es simetrica o no
        public void MatrixSimetric()
        {
            Console.WriteLine("Dimensions de la matriu: ");
            Console.Write("X: ");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Y: ");
            int y = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introdueix els valors: ");
            int[,] matrix = new int[y, x];
            int[,] opposite_matrix = new int[y, x];
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    matrix[i, j] = Convert.ToInt32(Console.ReadLine());
                    opposite_matrix[y - i - 1, x - j - 1] = matrix[i, j];
                }
            }
            bool isSimetric = true;
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    if (matrix[i, j] != opposite_matrix[i, j]) { isSimetric = false; }
                }
            }
            int count = 0;
            foreach (int val in matrix)
            {
                if (count == matrix.GetLength(0)) { Console.WriteLine(); count = 0; }
                Console.Write($"{val} ");
                count++;
            }
            Console.WriteLine("\n" + isSimetric);
        }

        // Multiplica dues matrius de mateixes dimensions quadrades
        public void MultiplicaMatrius()
        {
            Console.WriteLine("Dimensions de les matrius: ");
            Console.Write("X: ");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Y: ");
            int y = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Matriu 1 (Introdueix valor per valor)");
            int[,] matrix1 = new int[y, x];
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    matrix1[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            Console.WriteLine("Matriu 2 (Introdueix valor per valor)");
            int[,] matrix2 = new int[y, x];
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    matrix2[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            int[,] matrixRes = new int[y, x];
            for (int i = 0; i < y; i++)
            {
                for (int j = 0; j < x; j++)
                {
                    matrixRes[i, j] = 0;
                    for (int a = 0, b = 0; a < y && b < x; a++, b++)
                    {
                        matrixRes[i, j] += matrix1[i, a] * matrix2[b, j];
                    }
                }
            }
            int count = 0;
            foreach (int val in matrixRes)
            {
                if (count == matrixRes.GetLength(0)) { Console.WriteLine(); count = 0; }
                Console.Write($"{val} ");
                count++;
            }
        }

        // Coloca 8 reines en un tauler sense que es puguin tocar
        public void QueenGame()
        {
            int a = 0;
            int b = 0;
            bool available_space;
            char[,] chess = new char[8, 8];
            do
            {
                for (int x = 0; x < chess.GetLength(0); x++)
                {
                    for (int y = 0; y < chess.GetLength(1); y++) { chess[x, y] = 'x'; }
                }
                bool[] invalidI = { false, false, false, false, false, false, false, false }; // POSICIONS DEL TAULER, (TRUE = OCUPADA)
                bool[] invalidJ = { false, false, false, false, false, false, false, false };
                bool[] invalidDiagonalA = { false, false, false, false, false, false, false, false,
                false, false, false, false, false, false, false};
                bool[] invalidDiagonalB = { false, false, false, false, false, false, false, false,
                false, false, false, false, false, false, false};

                chess[a, b] = 'Q'; // INTRODUCIR REINA INICIAL
                invalidI[a] = true; invalidJ[b] = true;
                invalidDiagonalA[a - b + 7] = true; invalidDiagonalB[a + b] = true;

                int i = 0; int j = 0;
                for (; i < 8; i++) // INTRODUIR UNA NOVA REINA CADA VEGADA QUE TROBA UN LLOC DISPONIBLE
                {
                    if (!invalidI[i])
                    {
                        for (; j < 8; j++)
                        {
                            if (!invalidJ[j] && !invalidI[i] && !invalidDiagonalA[i - j + 7] && !invalidDiagonalB[i + j])
                            {
                                chess[i, j] = 'Q';
                                invalidJ[j] = true;
                                invalidI[i] = true;
                                invalidDiagonalA[i - j + 7] = true;
                                invalidDiagonalB[i + j] = true;
                            }
                        }
                        j = 0;
                    }
                }
                // DEBUG ////////////////////////////////////////////////////////////
                /*
                int c = 0;
                foreach (char val in chess)
                {
                    if (c == chess.GetLength(0)) { Console.WriteLine(); c = 0; }
                    Console.Write($"{val} ");
                    c++;
                }
                Console.WriteLine();
                for (i = 0; i < invalidI.Length; i++) { Console.Write($"{invalidI[i]}"); }
                Console.WriteLine();
                for (i = 0; i < invalidJ.Length; i++) { Console.Write($"{invalidJ[i]}"); }
                Console.WriteLine();
                */
                /////////////////////////////////////////////////////////////////////
                if (b < 7) { b++; } // ITERAR SOBRE LA REINA INICIAL
                else if (a < 7) { a++; b = 0; }
                else { a = 0; b = 0; }
                available_space = false; // CONTROLAR SI QUEDA ESPAI LLIURE AL TAULER
                foreach (bool val in invalidI) { if (val == false) { available_space = true; } }
            } while (available_space && a + b != 0);
            int count = 0;
            foreach (char val in chess)
            {
                if (count == chess.GetLength(0)) { Console.WriteLine(); count = 0; }
                Console.Write($"{val} ");
                count++;
            }
        }

        // Donat un tamany per una matriu, crea un quadrat mágic
        public void QuadratMagic()
        {
            Console.WriteLine("Dimensions de les matrius: ");
            Console.Write("X e Y: ");
            int x = Convert.ToInt32(Console.ReadLine());
            int[,] matrixRes = new int[x, x];
            // QUADRAT MÀGIC AMB IMPARELLS
            if (x % 2 == 1)
            {
                int extraSlots = x / 2;
                int[,] matrix = new int[x + (extraSlots * 2), x + (extraSlots * 2)];
                int half = x / 2 + extraSlots;
                for (int diagonal = 0, c = 1; diagonal < x; diagonal++)
                {
                    for (int i = 0 + diagonal, j = half - diagonal; i < x + diagonal; i++, j++, c++)
                    {
                        matrix[i, j] = c;
                    }
                }
                for (int i = 0; i < matrixRes.GetLength(0); i++)
                {
                    for (int j = 0; j < matrixRes.GetLength(1); j++)
                    {
                        matrixRes[i, j] = matrix[i + extraSlots, j + extraSlots];
                        if (matrixRes[i, j] == 0)
                        {
                            if (i - j < 0 && i + j < half) //TOP SECTION
                            {
                                matrixRes[i, j] = matrix[i + extraSlots + x, j + extraSlots];
                            }
                            if (i - j < 0 && i + j > half) //RIGHT SECTION
                            {
                                matrixRes[i, j] = matrix[i + extraSlots, j + extraSlots - x];
                            }
                            if (i - j > 0 && i + j > half) //BOTTOM SECTION
                            {
                                matrixRes[i, j] = matrix[i + extraSlots - x, j + extraSlots];
                            }
                            if (i - j > 0 && i + j < half) //LEFT SECTION
                            {
                                matrixRes[i, j] = matrix[i + extraSlots, j + extraSlots + x];
                            }
                        }
                    }
                }
            }
            // QUADRAT MÀGIC AMB PARELLS
            else
            {
                int[] vectorUsed = new int[x * 2];
                int vUsedIndex = 0;
                for (int i = 0, c = 1; i < matrixRes.GetLength(0); i++)
                {
                    for (int j = 0; j < matrixRes.GetLength(1); j++, c++)
                    {
                        if (i - j == 0 || i + j == x - 1)
                        {
                            matrixRes[i, j] = c;
                            vectorUsed[vUsedIndex] = c; // GUARDA EN UN VECTOR ELS NÚMEROS USATS EN LES DIAGONALS
                            vUsedIndex++;
                        }
                    }
                }
                for (int i = matrixRes.GetLength(0) - 1, c = 1; i >= 0; i--)
                {
                    for (int j = matrixRes.GetLength(1) - 1; j >= 0; j--, c++)
                    {
                        if (matrixRes[i, j] == 0)
                        {
                            bool isInVector;
                            do
                            {
                                isInVector = false;
                                foreach (int val in vectorUsed) { if (val == c) { isInVector = true; c++; } }
                            } while (isInVector == true); // SALTA ELS NÜMEROS QUE JA S'HAN INTRODUIT
                            matrixRes[i, j] = c;
                        }
                    }
                }
            }
            int count = 0;
            foreach (int val in matrixRes)
            {
                if (count == matrixRes.GetLength(0)) { Console.WriteLine("\n"); count = 0; }
                Console.Write($"\t{val}");
                count++;
            }
        }

        public bool Menu(bool end)
        {
            string[] exercicis = { " 0 - Exit", " 1 - SimpleBattleshipResult", " 2 - MatrixElementSum", " 3 - MatrixBoxesOpenedCounter",
                " 4 - MatrixThereADiv13", " 5 - HighestMountainOnMap", " 6 - HighestMountainScaleChange", " 7 - MatrixSum",
                " 8 - RookMoves", " 9 - MatrixSimetric","10 - MultiplicaMatrius", "11 - QueenGame", "12 - QuadratMagic"};
            Console.WriteLine("Quin exercici vols executar?\n");
            for (int i = 0; i < exercicis.Length; i++)
            {
                Console.WriteLine(exercicis[i]);
            }
            Console.Write("\n Escriu un número: ");
            string option = Console.ReadLine();
            Console.Clear();
            switch (option)
            {
                case "0":
                    end = !end;
                    break;
                case "1":
                    SimpleBattleshipResult();
                    break;
                case "2":
                    MatrixElementSum();
                    break;
                case "3":
                    MatrixBoxesOpenedCounter();
                    break;
                case "4":
                    MatrixThereADiv13();
                    break;
                case "5":
                    HighestMountainOnMap();
                    break;
                case "6":
                    HighestMountainScaleChange();
                    break;
                case "7":
                    MatrixSum();
                    break;
                case "8":
                    RookMoves();
                    break;
                case "9":
                    MatrixSimetric();
                    break;
                case "10":
                    MultiplicaMatrius();
                    break;
                case "11":
                    QueenGame();
                    break;
                case "12":
                    QuadratMagic();
                    break;
                default:
                    Console.WriteLine("\nOpció incorrecta!\n");
                    break;
            }
            Console.WriteLine("\n\n[PRESS ENTER TO CONTINUE]");
            Console.ReadLine();
            Console.Clear();
            return end;
        }
        static void Main()
        {
            var menu = new Matrius();
            bool end = false;
            while (!end) { end = menu.Menu(end); }
        }
    }
}
